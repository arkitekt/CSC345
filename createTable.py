from dbModels import *

class Create:

    def DATABASE(engine):
        print("Checking Databases...")
        existing_databases = engine.execute("SHOW DATABASES;")
        # Results are a list of single item tuples, so unpack each tuple
        existing_databases = [d[0] for d in existing_databases]
        database = "Market"
        # Create database if not exists
        if database not in existing_databases:
            engine.execute("CREATE DATABASE {0}".format(database))
            print("Created database : '{0}'".format(database))
        else:
            print("Database already exists, creating connection")
        print("Connection established")
        engine = create_engine(
            'mysql+pymysql://{0}:{1}@127.0.0.1/{2}?host=127.0.0.1?port={3}/charset=utf8/'.format(DBUNAME, DBPASS,
                                                                                                 database, DBPORT))
        return engine

    def TABLE(engine):
        Session = sessionmaker(bind=engine)
        # create a Session
        session = Session()
        try:
            client_tbl = session.query(Client_Table).first()
            print("Client_tbl already exists")
        except:
            print("Creating Client_tbl")
            # Create all tables in meta
            metaModels(engine)
            print("Client_tbl has been Created")
        return session

    def ADD_DATA():
        fname = "data.txt"
        with open(fname) as f:
            content = f.read().splitlines()

        print("Preparing Data... Stripping ~, 	 and fixing DATE")
        for data in content:
            """
            prepare data for sqlalchemy
            remove ~, 	 and move YEAR to front of DATE
            """

            data = str(data).replace("	", "")
            data = data.split("~")
            """
            fixes DATE by stripping year off the end
            and adding to beggining of DATE FOR DATE COLUMN
            """
            data[2] = data[2].replace("/2018", "")
            data[2] = "2018/" + str(data[2])

            newCleint = Client_Table(AutoNumber=data[0], OrderNumber=data[1], OrderDate=data[2], ClientNumber=data[3],
                                     ClientName=data[4])
            session.add(newCleint)
        session.commit()
        print("Clients added")


"""
Create Client_tbl
engine = Create.DATABASE(engine)
session = Create.TABLE(engine)
Create.ADD_DATA()
"""