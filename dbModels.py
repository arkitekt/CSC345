from sqlalchemy import create_engine, MetaData, Column, VARCHAR, INT, BIGINT, FLOAT, REAL, ForeignKey, Table, collate, DATE
from sqlalchemy import MetaData
from sqlalchemy.orm import sessionmaker, relationship
from sqlalchemy import Column
from sqlalchemy.ext.declarative import declarative_base
import logging
from credentials import *
base = declarative_base()
engine = create_engine('mysql+pymysql://{0}:{1}@127.0.0.1/{2}?host=127.0.0.1?port={3}/charset=utf8/'.format(DBUNAME, DBPASS, DB, DBPORT))
# create a configured "Session" class
conn = engine.connect()
Session = sessionmaker(bind=conn)
# create a Session
session = Session()

def metaModels(engine):
    meta = MetaData(engine)
    """
    # create Original Client Table from meta data
    client_tbl = Table('Client_tbl', meta,
               Column('AutoNumber', BIGINT, primary_key=True),
               Column('OrderNumber', BIGINT, default=None),
               Column('OrderDate', DATE, default=None),
               Column('ClientNumber', BIGINT, default=None),
               Column('ClientName', VARCHAR(50), default=None),
               mysql_engine='InnoDB',
               mysql_charset='utf8mb4'
               )
    """
    Order_tbl = Table('Order_tbl', meta,
                      Column('ClientNumber', BIGINT),
                      Column('OrderNumber', BIGINT, primary_key=True),
                      Column('OrderDate', DATE),
                      mysql_engine='InnoDB',
                      mysql_charset='utf8mb4'
                      )
    New_Client_tbl = Table('New_Client_tbl', meta,
                      Column('ClientNumber', BIGINT, primary_key=True),
                      Column('ClientName', VARCHAR(50), default=None),
                      mysql_engine='InnoDB',
                      mysql_charset='utf8mb4'
                      )
    # create all tables in meta
    meta.create_all()

class Client_Table(base):
    __tablename__ = "Client_tbl"
    AutoNumber = Column(BIGINT, primary_key=True)
    OrderNumber = Column(BIGINT, default=None)
    OrderDate = Column(DATE, default=None)
    ClientNumber = Column(BIGINT, default=None)
    ClientName = Column(VARCHAR(50), default=None)

    def __repr__(self):
        return "<Client_tbl(AutoNumber=%s, OrderNumber=%s, OrderDate=%s, ClientNumber=%s, ClientName=%s )>"\
               % (self.AutoNumber, self.OrderNumber, self.OrderDate, self.ClientNumber, self.ClientName)


class New_Client_Table(base):
    __tablename__ = "New_Client_tbl"
    ClientNumber = Column(BIGINT, primary_key=True)
    ClientName = Column(VARCHAR(50), default=None)
    Orders = relationship('Order_Table', backref='owner')

    def __repr__(self):
        return "<New_Client_tbl(ClientNumber=%s, ClientName=%s, Orders=%s)>" \
               % (self.ClientNumber, self.ClientName, self.Orders)



class Order_Table(base):
    __tablename__ = "Order_tbl"
    ClientNumber = Column(BIGINT, ForeignKey('New_Client_tbl.ClientNumber'))
    OrderNumber = Column(BIGINT, primary_key=True)
    OrderDate = Column(DATE)

    def __repr__(self):
        return "<Order_tbl(ClientNumber=%s, OrderNumber=%s, OrderDate=%s)>" \
               % (self.ClientNumber, self.OrderNumber, self.OrderDate)


