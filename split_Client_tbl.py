from dbModels import *

class Split:
    def __init__(self,session):
        self.session = session
        self.createNewTables()

    def createNewTables(self):
        print("Creating new Tables from Meta")
        metaModels(engine)
        print("Success")
        #split data to new tables
        Clients = session.query(Client_Table).all()
        print("Preparing Records")
        for i in Clients:
            checkClients = session.query(New_Client_Table).filter_by(ClientNumber=i.ClientNumber).first()
            if checkClients == None:
                Client = New_Client_Table(ClientName=i.ClientName, ClientNumber=i.ClientNumber)
                session.add(Client)
                session.commit()
                print(Client)
            else:
                print("Duplicate Client #", i.ClientNumber)

            checkOrders = session.query(Order_Table).filter_by(OrderNumber=i.OrderNumber).first()
            if checkOrders == None:
                Order = Order_Table(ClientNumber=i.ClientNumber, OrderNumber=i.OrderNumber, OrderDate=i.OrderDate)
                print(Order)
                session.add(Order)
                session.commit()
            else:
                print("Duplicate Order #", i.OrderNumber)

        print("Data split\n")
        return True


Split.createNewTables(session)


Orders = session.query(New_Client_Table).all()
print("Querying Client Orders by ClientNumber\n")
for i in Orders:
    if i.Orders != None:
        print("Client # ", i.ClientNumber, i.Orders)